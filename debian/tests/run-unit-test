#!/bin/bash
set -e

pkg="altree"

export LC_ALL=C.UTF-8
if [ "${AUTOPKGTEST_TMP}" = "" ] ; then
  AUTOPKGTEST_TMP=$(mktemp -d /tmp/${pkg}-test.XXXXXX)
  # Double quote below to expand the temporary directory variable now versus
  # later is on purpose.
  # shellcheck disable=SC2064
  trap "rm -rf ${AUTOPKGTEST_TMP}" 0 INT QUIT ABRT PIPE TERM
fi

cp -a /usr/share/doc/${pkg}/examples/* "${AUTOPKGTEST_TMP}"

cd "${AUTOPKGTEST_TMP}"
gunzip -r *

# phase input to ALTree
cd create_file/paup_file
altree-convert -i caco.phase.out -q "qualitative" -s 5 -p paup -t NUM -r phase -c nb_cas_control.txt -o caco.prepaup

# Associatation test for paup when ancestor is absent
cd ../../paup/ancestor_absent/association
altree -i test.res.log  -j nb_cas_control.txt -a -t SNP -p paup -r 1 --tree-to-analyse 1 -o 1_caco.asso -q qualitative

# Localisation test for paup when ancestor is absent
cd ../localisation
altree-add-S -i caco.paup -l 1 -j \
nb_cas_control.txt  -o et_caco.paup -e 1 -t SNP -p 0.58 \
-q qualitative

altree -i test.res.log \
-j nb_cas_control.txt  -t SNP -p paup  --tree-to-analyse 89 \
--s-site-number 13 --s-site-characters "0->1" \
--co-evo double -l -o caco.loc

# Association test for paup when ancestor present
cd ../../ancestor_present/localisation
altree -i test.res.log  -j nb_cas_control.txt -a -t SNP \
 -q qualitative -p paup -r 1 --tree-to-analyse 1 -o 1_caco.asso

altree -i test.res.log  -j nb_cas_control.txt -a -t SNP \
 -q qualitative -p paup  --tree-to-analyse 1 --chi2-threshold 0.05  -r 0 \
 -o 1_caco.asso

# Localisation test for paup when ancestor present
cd ../localisation
#   To optain the paup input file containing the S character
altree-add-S -i caco.paup -l 1 -q qualitative -j \
nb_cas_control.txt  -o et_caco.paup -e 1 -t SNP -p 0.5

#   To perform the localisation test on the 89 equiparsimonious trees
altree -i test.res.log  \
-j nb_cas_control.txt  -t SNP -p paup  --tree-to-analyse 89 \
--s-site-number 13 --s-site-characters "0->1" \
--co-evo double -l > caco.loc

# Association test for paup with outgroup absent
cd ../../outgr_absent/association
#   To perform the association test (on the first tree found in the file test.res.log)
altree -i test.res.log -q qualitative -j nb_cas_control.txt -a -t SNP \
 --remove-outgroup -p paup -r 1 --tree-to-analyse 1 -o 1_caco.asso

# Localisation test for paup with outgroup absent
cd ../localisation
#   To add the character S 
#   p=102 case haplotypes/176 haplotypes
#   The outgroup is not specified in the file nb_cas_control.txt. The 
#   option --outgroup must then be used
altree-add-S -i caco.paup -q qualitative -l 1 -j \
nb_cas_control.txt  -o et_caco.paup -e 1 -t SNP -p 0.58 \
--outgroup H000

# Association test for paup with outgroup present
cd ../../outgr_present/association
altree -i test.res.log -q qualitative -j nb_cas_control.txt -a -t SNP \
 -p paup -r 1 --tree-to-analyse 1 -o 1_caco.asso

# Localisation test for paup with outgroup present
cd ../localisation
#   To optain the paup input file containing the S character
altree-add-S -i caco.paup -l 1 -q qualitative -j \
nb_cas_control.txt  -o et_caco.paup -e 1 -t SNP -p 0.5

#   To perform the localisation test on the 89 equiparsimonious trees
altree -i test.res.log -q qualitative \
-j nb_cas_control.txt  -t SNP -p paup  --tree-to-analyse 89 \
--s-site-number 13 --s-site-characters "0->1" \
--co-evo double -l -o caco.loc

# Association test for paup with unrooted_absent
cd ../../unrooted_absent/association
# The outgroup is specified (option --outgroup) because the tree must be 
# rooted for the test but it is removed before the tree analysis
altree -i test.res.log -q qualitative -j nb_cas_control.txt -a -t SNP \
 --outgroup H000 --remove-outgroup -p paup  -r 1 --tree-to-analyse 1\
 -o 1_caco.asso

# Localisation test for paup with unrooted_absent
cd ../localisation
#   To obtain the paup input file containing the character S
altree-add-S -i caco.paup -l 1 -q qualitative -j nb_cas_control.txt \
  -o et_caco.paup -e 1 -t SNP -p 0.58 

# Association test for paup with unrooted_present
cd ../../unrooted_present/association
# only one permutation is done: the corrected p_value doesn't mean 
# anything!
# the --outgroup option is specified because the tree must be rooted for 
# the association test
altree -i test.res.log -q qualitative -j nb_cas_control.txt -a -t SNP \
 --outgroup H000 -p paup -r 1  --tree-to-analyse 1 -o 1_caco.asso

 # Localisation test for paup with unrooted_presnt
 #To obtain the paup input file containing the character S
cd ../localisation
altree-add-S -q qualitative -l 1 -i caco.paup -j \
nb_cas_control.txt  -o et_caco.paup -e 1 -t SNP -p 0.5 

# Association test for phylip with ancestor absent
cd ../../../phylip/ancestor_absent/association
# The ancestral sequence is not in the phylip output file, that's why we 
# have to specify it with the --anc-seq option 
# Only one permutation is performed...
altree -i outfile  -j nb_cas_controls.txt \
-a -t SNP -p phylip -r 1 --data-qual qualitative \
--tree-to-analyse 1 --anc-seq 1100010001 -o 1_trio_phy.asso

# Association test for phylip with ancestor present
cd ../../ancestor_present/association
# The ancestral sequence must be 
# provided with the --anc-seq option beacuse it is not in the phylip 
# output file
 altree -i outfile  -j nb_cas_controls.txt \
 -a -t SNP -p phylip -r 1 --data-qual qualitative\
 --tree-to-analyse 1 --anc-seq 1100010001 -o 1_trio_phy.asso

# Association test for phylip with outgroup absent
cd ../../outgroup_absent/association
# The outgroup is removed from the analysis (option 
# --remove-outgroup). The name of the outgroup must be specified so that 
# the program can identify the outgroup sequence
altree -i outfile  -j nb_cas_controls.txt --data-qual qualitative \
 -a -t SNP  -p phylip --outgroup OUTG --remove-outgroup  -r 1 \
 --tree-to-analyse 1  -o 1_trio_phy.asso

# Association test for phylip with outgroup present
cd ../../outgroup_present/association
# The outgroup is not removed, so it is not necessary to specify its name.
# Analysis on the first tree only
altree -i outfile  -j nb_cas_controls.txt \
 -a -t SNP -p phylip -r 1 --data-qual qualitative \
 --tree-to-analyse 1  -o 1_trio_phy.asso

# Association test for paml with unrooted absent
cd ../../../paml/unrooted_absent/association
altree -i rst -q qualitative -j nb_cas_control.txt  -a -t SNP \
 --remove-outgroup --outgroup OUTG  -p paml -r 1 -o 1_trio_ML.asso

 # Association test for paml with unrooted present
cd ../../unrooted_present/association
altree -i rst -j nb_cas_control.txt -q qualitative -a -t SNP \
		--outgroup OUTG -p paml -r 10 -o 1_trio_ML.asso --data-qual qualitative

